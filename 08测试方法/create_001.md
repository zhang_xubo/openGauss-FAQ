### Question

编译的数据库插件，将编译后的文件（.so文件、 .control文件和 .sql文件）放到对应的路径下，在om方式（xml方式安装）的数据库中使用插件，创建扩展失败，失败信息如下：

~~~sql
create extension xxx;   --xxx为插件名称
ERROR：could not load library "xxx" :$GAUSSHOME/lib/postgresql/xxx ：undefined symbol:*********
~~~

注：扩展相关信息：

~~~sql
--可用的扩展查询
select * from pg_available_extensions;
--已经安装的扩展
select * from pg_extension;  --对应元命令   \dx
--编译的扩展文件存放路径：
--1、sql和control文件
cd $GAUSSHOME/share/postgresql/extension
--2、so文件
cd $GAUSSHOME/lib/postgresql
~~~

### Anwser

编译数据库的时候没有加 --enable-mot参数

