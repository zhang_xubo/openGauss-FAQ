### Question

~~~
使用copy命令从文件中往表里复制数据，针对序列所在列如何处理？
copy的文件中并没有序列列的数据
~~~


### Anwser

copy的时候指定列（排除序列所在列），详细见如下案例：

~~~sql
--创建测试表
create table test(id bigserial,col1 text,col2 text);
--表结构信息如下
postgres=#\d+ test
                                              Table "public.test"
+--------+--------+---------------------------------------------------+----------+--------------+-------------+
| Column |  Type  |                     Modifiers                     | Storage  | Stats target | Description |
+--------+--------+---------------------------------------------------+----------+--------------+-------------+
| id     | bigint | not null default nextval('test_id_seq'::regclass) | plain    |              |             |
| col1   | text   |                                                   | extended |              |             |
| col2   | text   |                                                   | extended |              |             |
+--------+--------+---------------------------------------------------+----------+--------------+-------------+
Has OIDs: no
Options: orientation=row, compression=no
--创建copy的测试文件
vi /tmp/data
tabo	 tabo
gvum	 gvum
pohv	 pohv
okgv	 okgv
dsgh	 dsgh
rbwv	 rbwv
dopy	 dopy
cysd	 cysd
ppyy	 ppyy
lrzg	 lrzg
phkk	 phkk
aulp	 aulp
mkyp	 mkyp
ucug	 ucug
nouu	 nouu
vase	 vase
khga	 khga
surh	 surh
ngec	 ngec
rmbe	 rmbe
--使用copy命令复制数据
copy test(col1,col2) FROM '/tmp/data' ;
copy test(col1,col2) FROM '/tmp/data'  WITH (format 'text', delimiter E'\t', ignore_extra_data 'true', noescaping 'true');
--执行结果
postgres=#copy test(col1,col2) FROM '/tmp/data' ;
COPY 20
postgres=#select * from test;
+----+------+-------+
| id | col1 | col2  |
+----+------+-------+
|  1 | tabo |  tabo |
|  2 | gvum |  gvum |
|  3 | pohv |  pohv |
|  4 | okgv |  okgv |
|  5 | dsgh |  dsgh |
|  6 | rbwv |  rbwv |
|  7 | dopy |  dopy |
|  8 | cysd |  cysd |
|  9 | ppyy |  ppyy |
| 10 | lrzg |  lrzg |
| 11 | phkk |  phkk |
| 12 | aulp |  aulp |
| 13 | mkyp |  mkyp |
| 14 | ucug |  ucug |
| 15 | nouu |  nouu |
| 16 | vase |  vase |
| 17 | khga |  khga |
| 18 | surh |  surh |
| 19 | ngec |  ngec |
| 20 | rmbe |  rmbe |
+----+------+-------+
(20 rows)

postgres=#copy test(col1,col2) FROM '/tmp/data'  WITH (format 'text', delimiter E'\t', ignore_extra_data 'true', noescaping 'true');
COPY 20
postgres=#select * from test;
+----+------+-------+
| id | col1 | col2  |
+----+------+-------+
|  1 | tabo |  tabo |
|  2 | gvum |  gvum |
|  3 | pohv |  pohv |
|  4 | okgv |  okgv |
|  5 | dsgh |  dsgh |
|  6 | rbwv |  rbwv |
|  7 | dopy |  dopy |
|  8 | cysd |  cysd |
|  9 | ppyy |  ppyy |
| 10 | lrzg |  lrzg |
| 11 | phkk |  phkk |
| 12 | aulp |  aulp |
| 13 | mkyp |  mkyp |
| 14 | ucug |  ucug |
| 15 | nouu |  nouu |
| 16 | vase |  vase |
| 17 | khga |  khga |
| 18 | surh |  surh |
| 19 | ngec |  ngec |
| 20 | rmbe |  rmbe |
| 21 | tabo |  tabo |
| 22 | gvum |  gvum |
| 23 | pohv |  pohv |
| 24 | okgv |  okgv |
| 25 | dsgh |  dsgh |
| 26 | rbwv |  rbwv |
| 27 | dopy |  dopy |
| 28 | cysd |  cysd |
| 29 | ppyy |  ppyy |
| 30 | lrzg |  lrzg |
| 31 | phkk |  phkk |
| 32 | aulp |  aulp |
| 33 | mkyp |  mkyp |
| 34 | ucug |  ucug |
| 35 | nouu |  nouu |
| 36 | vase |  vase |
| 37 | khga |  khga |
| 38 | surh |  surh |
| 39 | ngec |  ngec |
| 40 | rmbe |  rmbe |
+----+------+-------+
(40 rows)
~~~

如上两条copy命令都能将data文件中的数据复制到test表里面，复制的时候test表后面加上具体的字段名（忽略序列所在列即可），copy的具体选项见官方文档[COPY | openGauss](https://opengauss.org/zh/docs/3.0.0/docs/Developerguide/COPY.html)

