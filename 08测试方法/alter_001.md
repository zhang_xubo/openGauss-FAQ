### Question

修改初始化用户密码报错，报错信息如下：

~~~shell
opengauss=#alter user xxx with password 'Test123~';       #xxx表示初始用户
ERROR:  The old password can not be NULL, please input your old password with 'replace' grammar.
~~~

提示需要使用replace语法，也就是需要指定原始密码，如果原始密码忘记了随便指定也是会报错的：

~~~shell
opengauss=#alter user xxx identified by 'Test123~' replace '*******';
ERROR:  The old password is invalid.
~~~


### Anwser

​		出现这个报错的版本应该是官网的openGauss 2.1版本，这个问题在最新的代码中已经修复，初始用户修改自己的密码无需旧密码。

​		而且初始用户只允许在服务器本机登录，而本机登录初始用户可以配置trust认证方式，也就是初始用户忘记忘记密码也不影响使用，也就无需改密码了。

