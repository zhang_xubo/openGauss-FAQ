### Question

带CM组件的一主二备环境所有数据库节点ReadOnly

~~~sql
node      node_ip         instance                          state
-------------------------------------------------------------------
1  node1 192.168.1.101  1    /opt/cmserver/cm_server Standby
2  node2 192.168.1.102  2    /opt/cmserver/cm_server Primary
3  node3 192.168.1.103  3    /opt/cmserver/cm_server Standby

[   Cluster State   ]

cluster_state   : Degraded
redistributing  : No
balanced        : No
current_az      : AZ_ALL

[  Datanode State   ]

node      node_ip         instance            state            | node      node_ip         instance            state            | node      node_ip         instance            state
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1  node1 192.168.1.101  6001 /opt/data P Standby ReadOnly | 2  node2 192.168.1.102  6002 /opt/data S Primary ReadOnly | 3  node3 192.168.1.103  6003 /opt/data S Standby ReadOnly
~~~

### Anwser

查看后台日志如下

~~~shell
2022-10-15 11:01:23.989 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:01:23.989 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:01:23.989 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:01:23.989 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:01:23.989 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:01:23.989 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:01:23.989 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:01:23.989 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:01:23.989 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:01:23.989 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:02:23.995 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:02:23.995 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:02:23.995 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:02:23.995 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:02:23.995 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:02:23.995 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:02:23.995 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:02:23.995 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:03:24.001 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:03:24.002 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:03:24.002 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:03:24.002 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:03:24.002 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:03:24.002 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:03:24.002 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:03:24.002 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:04:24.008 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:04:24.008 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:04:24.008 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:04:24.008 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:04:24.008 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:04:24.008 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:04:24.008 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:04:24.008 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:05:24.016 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:05:24.016 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:05:24.016 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:05:24.016 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:05:24.016 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:05:24.016 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:05:24.016 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:05:24.016 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:06:24.022 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:06:24.023 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:06:24.023 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:06:24.023 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:06:24.023 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:06:24.023 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:06:24.023 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:06:24.023 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
2022-10-15 11:07:24.029 tid=210438  LOG: [StorageDetectMain][line:752] Parameter values of data disk check,enable_transaction_read_only=on,datastorage_readonly_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = on',datastorage_readwrite_set=gs_guc reload -Z datanode -N %s -D %s -c 'default_transaction_read_only = off',datastorage_threshold_check_interval=10,max_datastorage_threshold_check=43200,g_enableSetReadOnlyThreshold=85.
2022-10-15 11:07:24.030 tid=210438  LOG: [StorageDetectMain][line:779] role:[1]
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:596] [Disk usage] Starting check for disk alarm, preAlarmThreshhold=68.
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node1, usage=85.
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6001, usage=85
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node2, usage=83.
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6002, usage=83
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:608] [logDisk usage] Alarm threshold reached, nodeName=node3, usage=83.
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:629] [dataDisk usage] Alarm threshold reached, instanceId=6003, usage=83
2022-10-15 11:07:24.030 tid=210438  LOG: [PreAlarmForNodeThreshold][line:638] [Disk usage] Check for disk alarm done.
2022-10-15 11:07:24.030 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:399] check storage read only start, threshold=85 
2022-10-15 11:07:24.030 tid=210438  LOG: [CheckAndSetStorageThresholdReadOnlyAlarm][line:432] Set database to read only mode, instanceId=6001, usage=85
2022-10-15 11:07:24.030 tid=210438  LOG: [SetNodeReadOnlyStatusByInstanceId][line:365] groupDnNum=3, instanceId=6001
2022-10-15 11:07:24.030 tid=210438  LOG: [SaveNodeReadOnlyConfig][line:235] bitsString = 1110, bitIndex = 4 alarmIndex: 3.
2022-10-15 11:07:24.030 tid=210438  LOG: [SetNodeReadOnlyStatusToDdb][line:1936] bitsString:1110, valueOfReadOnlyStatus:e, strlen(bitsString):4
~~~

磁盘使用率达到了85%达到阈值，数据库自动设置只读模式，磁盘扩容即可自动恢复正常。



