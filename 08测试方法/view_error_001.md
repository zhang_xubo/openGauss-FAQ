### Question

初始用户使用pg_total_memory_detail视图，查询内存相关的信息，报错如下

~~~sql
omm@postgres=#select * from pg_total_memory_detail;
ERROR:  unsupported view for memory protection feature is disabled.
omm@postgres=#
~~~

### Anwser

受参数enable_memory_limit影响，当参数值为off时，会出现如上报错，修改参数值为on即可

~~~sql
omm@postgres=#select name,setting,context from pg_settings where name ~ 'enable_memory_limit';
+---------------------+---------+------------+
|        name         | setting |  context   |
+---------------------+---------+------------+
| enable_memory_limit | off     | postmaster |
+---------------------+---------+------------+
~~~

如上该参数为postmaster级别的参数，修改后需要重启数据库才生效

~~~sql
gs_guc reload -N all -I all -c "enable_memory_limit=on"
gs_om -t restart
gs_guc check -N all -I all -c "enable_memory_limit"
~~~





