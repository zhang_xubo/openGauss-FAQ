### Question

使用xml方式安装数据库，预安装阶段报错如下：

~~~shell
[GAUSS-52301] : Failed to check OS patameters. The value of removeIPC must be no.
~~~

### Anwser

1. 设置为非no的话就会无规律coredump，所以现在数据库安装会校验此参数

2. 修改：

   1. set RemoveIPC=no in /etc/systemd/logind.conf

   2. Reboot the server or restart systemd-logind as follows:

      systemctl daemon-reload

      systemctl restart systemd-logind
      
   3. 查看
      
      systemctl show systemd-logind | grep RemoveIPC
      
      1. 添加RemoveIPC=no 到 /usr/lib/systemd/system/systemd-logind.service
      
      2. 查看
      
         loginctl show-session | grep RemoveIPC











