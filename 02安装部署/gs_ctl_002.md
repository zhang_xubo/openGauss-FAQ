### Question

主备状态正常的情况下，备机执行failover

~~~shell
#XXX 为数据目录
gs_ctl failover -D XXX
gs_om -t refreshconf
~~~

以上两条命令执行成功出现双主情况

然后在任意一主节点上降为备机：

1. 确定降为备机的节点，在节点上执行如下命令关闭服务。

   ```
   gs_ctl stop -D XXX
   ```

2. 执行以下命令，以standby模式启动备节点。

   ```
   gs_ctl start -D XXX -M standby
   ```

以standby模式启动备节点的时候报错，启动失败

此时查看主备状态提示：Standby Need repair(WAL)

### Anwser

在备节点上执行rebuild命令

~~~shell
gs_ctl build -D XXX
~~~

此时再次查看主备状态，备库状态恢复正常



