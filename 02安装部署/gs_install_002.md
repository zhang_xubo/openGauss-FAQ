### Question

使用xml方式安装数据库，安装一主二备环境预安装阶段报错，报错信息如下：

~~~shell
[GAUSS-51400]：Failed to execute the command：rm -rf '/tmp/step_preinstall_file.dat'.Result:{'hostname1':'Failure','hostname2':'Success','hostname3':'Success'}

[GAUSS-51400]：Failed to execute the command：sed -i '/^export[ ]*HOST_IP=/d' /etc/profile .Result:{'hostname1':'Failure','hostname2':'Success','hostname3':'Success'}
~~~

### Anwser

这是节点见的互信问题，排查root用户和子用户的互信。