### Question

xml方式安装一主二备环境，预安装阶段报错，报错信息如下：

~~~shell
[GAUSS-51400]: Failed to execute the command: python3 '/xx/xx/xx/script/gs_checkos' -h hostname1,hostname2,hostname3 -i A -l '/xx/xx/xx/log/omm/kyleze/om/gs_local.log' -X '/xx/xx/xx/*.xml(此处为xml文件)' Error：
Checking items:
A6. [System control parameter status]     : Warning
[GAUSS-51632]: Failed to do python3 '/xx/xx/xx/script/local/LocalCheckOS.py' -t Check_Logical_Block -l '/xx/xx/xx/log/omm/kyleze/om/gs_local.log'. Errror:

#手动执行gs_checkos报错如下：
[root@hostname1 script]# python3 '/xx/xx/xx/script/gs_checkos' -h hostname1,hostname2,hostname3 -l '/xx/xx/xx/log/omm/kyleze/om/gs_local.log' -X '/xx/xx/xx/*.xml -i A6 --detail
Checking items:
A6. [System control parameter status]     : Abnormal
	[hostname1]
	Abnormal reason: variable 'net.ipv4.ip_local_reserved_ports' RealValue '20022-20029,20050-20057' ExpectedValue '16410-16417,20050-20057'
	Check——SysCtl_Patameter failed.
	
	[hostname2]
	Abnormal reason: variable 'net.ipv4.ip_local_reserved_ports' RealValue '20022-20029,20050-20057' ExpectedValue '16410-16417,20050-20057'
	Check——SysCtl_Patameter failed.

	[hostname3]
	Abnormal reason: variable 'net.ipv4.ip_local_reserved_ports' RealValue '20022-20029,20050-20057' ExpectedValue '16410-16417,20050-20057'
	Check——SysCtl_Patameter failed.
~~~



### Anwser

~~~shell
#修改操作系统的如下参数
[root@home data]# cat /etc/sysctl.conf |grep net.ipv4.ip_local_reserved_ports
net.ipv4.ip_local_reserved_ports = 20050-20057,60000-60007
[root@home data]# cat /proc/sys/net/ipv4/ip_local_reserved_ports
20050-20057,60000-60007
[root@home data]#
#修改完成后重新预安装，还报错，检查发现操作系统的参数自动还原了


#最终处理，修改三个节点安装代码的check_list.conf文件，将net.ipv4.ip_local_reserved_ports这一行注释掉
vim /xx/xx/xx/script/gspylib/etc/conf/check_list.conf
#net.ipv4.ip_local_reserved_ports = 16410-16417,20050-20057
~~~































































































