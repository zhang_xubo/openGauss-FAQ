### Question

xml方式安装一主二备，执行预安装脚本的时候报错，报错如下：

~~~shell
[FAILURE] <此处为节点的主机名>:
[GAUSS-51251] : The /<此处为xml中配置的路径信息>/gaussdb_log cannot be a root user group or a link
bash: /home/<此处为omm用户名>/<此处为环境变量分离的env文件名>: No such file or directory
~~~

### Anwser

报错原因未分析出来

使用以下方式处理后再次执行预安装脚本不再报错：

~~~
1、删除omm用户，然后删除omm相关的目录（家目录、邮件目录等）
2、删除预安装过程中产生的文件和文件夹
~~~







