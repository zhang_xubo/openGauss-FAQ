### Question

在centos7.8操作系统上，以xml方式安装  openGauss2.x版本数据库，预安装报错：

~~~shell
GAUSS-51900:"The current OS is not supported." The current system is : centos7.8
~~~

### Anwser

openGauss 2.x 部分版本不支持 centos 7.6以上的版本。

需要修改以下两个文件避开这个校验

~~~shell
#解压openGauss-2.x.x-CentOS-64bit-om.tar.gz文件后，进入解压后的目录
vim script/local/LocalCheckOS.py
1915     elif (data.distname in ("redhat", "centos", "asianux")):
1916         if (data.version in ("6.4", "6.5", "6.6", "6.7", "6.8", "6.9")):
1917             mixedType = "%s6" % data.distname
1918             platformStr = "%s_%s_%s" % (data.distname,
1919                                         data.version, data.bits)
1920         elif (data.version[0:3]
1921               in ("7.0", "7.1", "7.2", "7.3", "7.4", "7.5", "7.6")):
#如上注意1921行，把7.8编号加进来

vim script/gspylib/os/gsplatform.py
107 # RedhatX platform
108 SUPPORT_RHEL_SERIES_PLATFORM_LIST = [REDHAT, CENTOS, "kylin", "asianux"]
109 SUPPORT_RHEL6X_VERSION_LIST = ["6.4", "6.5", "6.6", "6.7", "6.8", "6.9", "10"]
110 SUPPORT_RHEL7X_VERSION_LIST = ["7.0", "7.1", "7.2", "7.3", "7.4", "7.5", "7.6", "10"]
#如上注意110行把7.8编号加进来
~~~

文件修改好之后，删除之前预安装对应路径下产生的文件，然后重新预安装即可。











