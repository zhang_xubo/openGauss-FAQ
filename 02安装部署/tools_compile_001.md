### Question

编译方式安装openGauss数据库后，使用相关第三方库编译其他插件，进入插件代码目录下，执行make命令，报错如下：

~~~
g++ error: unrecognized command line option '-std=c++14'
make[1]: ***[analyze.o] Error 1
make[1]: Leaving directory '插件代码目录'
make: ***[parser] Error 2
~~~

### Anwser

是在omm用户下执行的make命令，没有source环境变量（环境变量分离为了单独的文件），source环境变量之后再执行make命令即可。



