### Question

编译方式安装openGauss数据库后，初始化数据库成功，使用gs_ctl命令启动数据库服务的时候，报错如下：

~~~
could not open lock file "/tmp/.s.PGSQL.5432.lock" Permission denied
~~~

### Anwser

编译方式安装openGauss数据库默认使用的端口是5432，而5432端口可能被占用

~~~shell
#首先找一个没有被占用的端口
#修改postgresql.conf文件中的port值
vim postgresql.conf
port = xxxx
#然后gs_ctl命令启动数据库服务
~~~







