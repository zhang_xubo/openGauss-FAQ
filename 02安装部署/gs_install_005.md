### Question

xml方式安装主备环境，预安装阶段没有问题，安装阶段报错，报错信息如下： 

~~~shell
Checking the installation environment on all nodes. 
[GAUSS-51400]: Failed to execute the command:source /home/xxx/此处是环境变量文件; python3 '/xx/xx/xx/cluster/tool/script/local/CheckInstall.py' -U 用户名:组名 -R /xx/xx/xx/cluster/app -l -R /xx/xx/xx/cluster/guassdb_log/xx/om/gs_local.log -X /xx/xx/xx/*.xml(xml配置文件). Error: 
...... 
Checking instance port and IP. 
Number of processes must be as least 1 
~~~



### Anwser

原因： xml配置文件中参数名称输入错误 

~~~xml
<PARAM name="dataPortStandby" value="42175"/> 
~~~

如上dataPortStandby表示备库的端口号，
该报错原因是将该参数名称写为了data4PortStandby

