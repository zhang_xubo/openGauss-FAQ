### Question

xml方式安装单节点数据库成功（中途没有报错），但是使用gs_om工具查看数据库状态无信息 

~~~shell
 [omm@home dn_6001]$ gs_om -t status --detail
[   Cluster State   ]

cluster_state   : Normal
redistributing  : No
current_az      : AZ_ALL

[  Datanode State   ]

    nodenode_ip         port      instance  
~~~

使用gs_om工具启动数据库报错，报错信息如下：

~~~shell

[GAUSS-53600]:Can not start the database,the cmd is source /xxx/xxx/env(此处为变量分离的环境变量文件);python3 '/xx/xx/xx/cluster/tool/script/local/StartInstance.py' 
[FAILURE] hostname: 
[GAUSS-53610]:The input dataDir() may be incorrect.. 
~~~

### Anwser

xml配置文件有问题，配置dn目录的参数名称输入有误

~~~xml
<PARAM name="dataNode1" value="/data/openGauss/dn"/>
~~~

如上DN目录正确的参数名称为dataNode1，本次报错原因为该参数名称输入错误