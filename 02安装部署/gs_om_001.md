### Question

使用gs_om工具启动数据库报错，报错关键信息如下：

~~~
FATAL:data directory "/data/*/dn1" has group or world access
DETAIL:permission should be u=rwx (700)
~~~

### Anwser

数据目录的权限过大，权限应该为700，修改权限为：

~~~shell
chmod -R 700 /data/*/dn1
~~~

重新启动即可





