### Question

华为云服务器上，xml方式安装单机环境，预安装阶段没有问题，安装阶段报错，报错信息如下： 

~~~shell
[GAUSS-50601]: The port [0] is occupied or the ip address is incorrectly configured.
~~~



### Anwser

xml里面配置的是云服务器的公网IP，应该使用私网IP