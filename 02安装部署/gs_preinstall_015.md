### Question

云服务器上，xml方式安装单机环境，预安装阶段报错，报错信息如下：

~~~shell
[GAUSS-52400] : Installation environment does not meet the desired result.
Please get more details by "/data/pkg/script/gs_checkos -i A -h hostname --detail".
[root@hostname script]# /data/pkg/script/gs_checkos -i A -h hostname --detail
Checking items:
    A1. [ OS version status ]                                   : Normal
        [hostname]
        centos_7.6.1810_64bit

    A2. [ Kernel version status ]                               : Normal
        The names about all kernel versions are same. The value is "3.10.0-1160.53.1.el7.x86_64".
    A3. [ Unicode status ]                                      : Normal
        The values of all unicode are same. The value is "LANG=en_US.UTF-8".
    A4. [ Time zone status ]                                    : Normal
        The informations about all timezones are same. The value is "+0800".
    A5. [ Swap memory status ]                                  : Normal
        The value about swap memory is correct.
    A6. [ System control parameters status ]                    : Warning
        [hostname]
        Warning reason: variable 'net.ipv4.tcp_retries1' RealValue '3' ExpectedValue '5'.
        Warning reason: variable 'net.ipv4.tcp_syn_retries' RealValue '6' ExpectedValue '5'.
        Check_SysCtl_Parameter warning.

    A7. [ File system configuration status ]                    : Warning
        [hostname]
        Warning reason: variable 'open files' RealValue '65535' ExpectedValue '1000000'
        Warning reason: variable 'max user processes' RealValue '15073' ExpectedValue 'unlimited'

    A8. [ Disk configuration status ]                           : Normal
        The value about XFS mount parameters is correct.
    A9. [ Pre-read block size status ]                          : Normal
        The value about Logical block size is correct.
    A10.[ IO scheduler status ]                                 : Normal
        The value of IO scheduler is correct.
    A11.[ Network card configuration status ]                
      A12.[ Time consistency status ]                             : Warning
        [hostname]
        The NTPD not detected on machine and local time is "2022-10-12 11:23:18".

    A13.[ Firewall service status ]                             : Normal
        The firewall service is stopped.
    A14.[ THP service status ]                                  : Abnormal
        [hostname]
The THP service status RealValue 'enabled' ExpectedValue 'disabled'.

Total numbers:14. Abnormal numbers:1. Warning numbers:4.
Do checking operation finished. Result: Abnormal.
~~~



### Anwser

如上可以看出

/data/pkg/script/gs_checkos 检查环境信息的时候A14检查项没有通过

gs_checkos脚本中中有针对每项的修复方式

~~~shell
[root@home unzip]# cat script/gs_checkos|grep A14
    'A14': ['Checking items', '[ THP service status ]', 'Normal', 'OK', 'OK'],
                     'A10', 'A11', 'A12', 'A13', 'A14']
  'A14':[ THP service status ]
    elif (itemNumber == 'A14'):
                        "The THP service is stopped.", "A14", "Abnormal")
[root@home unzip]# cat script/gs_checkos|grep "THP service"
    'A14': ['Checking items', '[ THP service status ]', 'Normal', 'OK', 'OK'],
    'B6': ['Setting items', '[ Set THP service ]', 'Normal', 'OK', 'OK'],
  'A14':[ THP service status ]
  'B6':[ Set THP service ]
    Checking THP service
    g_logger.debug("Checking THP service.")
                        "The THP service is stopped.", "A14", "Abnormal")
    g_logger.debug("Successfully checked THP service.")
    Setting THP service
    g_logger.debug("Setting THP service.")
    g_logger.debug("Sucessfully setted THP service.")
[root@home unzip]#
~~~

如上可以看出针对A14的修改项是B6

使用以下命令修复：

~~~shell
./script/gs_checkos -i B6
~~~































































































