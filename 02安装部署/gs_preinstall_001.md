### Question

使用xml方式安装openGauss数据库，执行预安装脚本（./gs_preinstall）报错，报错信息如下：

[GAUSS-51405] : You need to install Software:expect

### Anwser

xml方式安装，采用交互模式执行前置命令时，需要事先安装expect包，命令如下：

~~~shell
yum install -y expect
~~~





