### Question

使用xml方式安装数据库，gs_install时，第二次输入数据库密码后，命令退出，提示如下：

~~~shell
'utf-8' codec can′t decode byte 0xcf in position 164: invalid continuation byte
~~~

### Anwser

~~~shell
#使用以下命令查看是否有乱码，有乱码说明有问题
gs_guc encrypt -M server -K '密码' -D DN目录
~~~

密码长度等于16位，可能有该问题，密码低于16位就可以了，后续会优化相关代码。

