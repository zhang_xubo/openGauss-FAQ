### Question

使用xml方式安装主备数据库，预安装阶段报错如下：

~~~shell
[GAUSS-52400] :Installation environment does not meet the desired result.
Please get more details by "xxx/script/gs_checkos -i A -h hostname1,hostname2 --detail"
You have mail in /var/spool/mail/root
~~~

### Anwser

原因：mail中的相关信息未删除干净

处理：

1、userdel -r username

2、查看mail中有无该用户信息（没有则表示已删除干净）

cd /var/spool/mail

3、重新进行预安装







