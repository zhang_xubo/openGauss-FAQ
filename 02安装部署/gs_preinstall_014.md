### Question

xml方式安装一主二备环境，预安装阶段报错，报错信息如下：

~~~shell
[root@testa script]# ./gs_preinstall -U omm -G dbgrp -X /tmp/2standby.xml
Parsing the configuration file.
Successfully parsed the configuration file.
[GAUSS-50217] : Failed to decompress version.cfg.The cmd is cd /software/unzip/script/../ && tar -xpf `ls openGauss*.tar.bz2|tail -1` ./version.cfg ./bin/encrypt && mv ./bin/encrypt ./script/gspylib/clib && rm -rf ./bin. The output is tar (child): lbzip2: Cannot exec: No such file or directory
tar (child): Error is not recoverable: exiting now
tar: Child returned status 2
tar: Error is not recoverable: exiting now.
[root@testa script]# tar
tar: You must specify one of the `-Acdtrux' or `--test-label'  options
Try `tar --help' or `tar --usage' for more information.
~~~



### Anwser

在linux系统上执行tar解压后缀为".bz2"的文件时出现“bzip2 执行失败，没有该文件或目录”，实际上是没有安装bzip2安装插件。

~~~shell
#安装 bzip2包
yum install -y bzip2
~~~































































































