### Question

使用gs_om工具启动数据库报错，报错关键信息如下：

~~~
FATAL:could not create shared memory segment:Cannot allocate memory
~~~

### Anwser

物理内存不够不能创建共享内存

检查操作系统问题，是否服务器异常

或者

~~~
echo 3 > /proc/sys/vm/drop_caches
ipcs -m|awk '$2 ~/[0-9]+/ {print $2}'|while read s;do ipcrm -m $s;done
~~~

或者考虑增加交换内存（虚拟内存），可参考以下资料：

[Linux 修改swap虚拟内存_Timber.Wang的博客-CSDN博客_linux设置swap内存](https://blog.csdn.net/Timber_kito/article/details/117599694)









