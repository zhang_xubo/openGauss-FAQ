### Question

xml方式安装一主二备数据库，预安装阶段成功，安装阶段报错如下：

~~~shell
[FAILURE] hostname1:
scp: error while loading shared libraries: libcrypto.so.1.1: cannot open shared object file: No such file or directory
[FAILURE] hostname2:
scp: error while loading shared libraries: libcrypto.so.1.1: cannot open shared object file: No such file or directory
[FAILURE] hostname3:
scp: error while loading shared libraries: libcrypto.so.1.1: cannot open shared object file: No such file or directory
~~~

### Anwser

无法加载环境变量，找不到包中的libcrypto.so.1.1文件，手动做一个软连接

~~~shell
ln -s /xxx/script/gspylib/clib/libcrypto.so.1.1 /lib64/libcrypto.so.1.1
~~~

