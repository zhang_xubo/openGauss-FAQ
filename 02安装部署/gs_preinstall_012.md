### Question

openEuler x64 环境使用xml方式安装主备数据库，预安装阶段报错如下：

~~~shell
[GAUSS-51400]:Failed to execute the command: python3 '/xxx/xxx/script/local/PreInstallUtility.py' -t check_config -u username -X /xxx/xxx/*.xml. Result:{'hostname1':'Success','hostname2':'Failure','hostname3':'Failure'}
Error:
[SUCCESS] hostname1:
bash: /xxx/xxx/此处为分离的环境变量文件名称: No such file or directory
[SUCCESS] hostname2:
bash: /xxx/xxx/此处为分离的环境变量文件名称: No such file or directory
[FAILURE] hostname3:
bash: line 1: /xxx/xxx/此处为分离的环境变量文件名称: No such file or directory

ImportError: libpython3.7m.so.1.0: cannot open shared object file:No such file or directory
~~~

### Anwser

编译安装Python3的时候，configure是要带参数--enable-shared，否则就会有这个报错

~~~shell
#安装（编译）
python3 tar -xvJf Python-3.7.1.tar.xz 
cd Python-3.7.1 
./configure --enable-shared 
make && make install 
ln /usr/local/bin/python3.7 /usr/bin/python3.7 
#卸载python3 
rpm -qa|grep python3|xargs rpm -ev --allmatches --nodeps 
whereis python3 |xargs rm -frv 
whereis python3
~~~









