### Question

使用xml方式安装openGauss数据库，执行预安装脚本（./gs_preinstall）报错，报错信息如下：

~~~shell
[GAUSS-51400] : Failed to execute the command: su - omm -c 'cd '/opengauss/log/omm''. Error:
~~~

### Anwser

权限不对，逐层检查错误提示的对应的路径信息

~~~shell
[root@primary script]# ls -ld /opengauss/log/omm
drwx------ 3 omm dbgrp 16 Apr 20 22:49 /opengauss/log/omm
[root@primary script]# ls -ld /opengauss/log
drwx------ 3 omm dbgrp 17 Apr 20 22:49 /opengauss/log
[root@primary script]# ls -ld /opengauss
drwx------ 3 root root 17 Apr 20 22:51 /opengauss
~~~

如上可以看到/opengauss目录的权限不对，切换到omm用户后没有权限

修改权限：

~~~shell
chmod o=rwx /opengauss
OR
chown omm:dbgrp /opengauss
~~~





