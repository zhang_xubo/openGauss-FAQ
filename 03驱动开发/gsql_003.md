### Question

gsql客户端，连接远程的数据库报错，报错命令如下：

~~~shell
#xx.xx.xx.xx表示远程数据库的IP，port表示远程数据库的端口号,-U 后面跟要连接的用户信息
gsql -U test -h xx.xx.xx.xx -p port -d postgres -r
FATAL: Forbid remote connection with trust method.
~~~


### Anwser

远程连接openGauss数据库，数据库服务端的白名单（pg_hba.conf）中针对gsql所在服务器IP的认证方式不能是trust。

修改数据库服务端的白名单（pg_hba.conf）中针对gsql所在服务器IP的认证方式为sha256，然后重启数据库再次远程连接即可

