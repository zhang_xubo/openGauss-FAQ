### Question

gsql客户端，连接远程的数据库报错，报错命令如下：

~~~shell
#xx.xx.xx.xx表示远程数据库的IP，port表示远程数据库的端口号
gsql -h xx.xx.xx.xx -p port -d postgres -r
FATAL: no pg_hba.conf entry for host "xx.xx.xx.xx"
~~~


### Anwser

远程数据库的白名单（pg_hba.conf）中没有gsql所在服务器的IP地址

在远程数据库服务器中的pg_hba.conf中加入以下配置：

~~~
echo 'host all all xx.xx.xx.xx/32 sha256' >> pg_hba.conf
~~~

然后重启数据库服务，然后再次远程连接数据库即可。