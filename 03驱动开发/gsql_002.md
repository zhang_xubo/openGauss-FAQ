### Question

gsql客户端，连接远程的数据库报错，报错命令如下：

~~~shell
#xx.xx.xx.xx表示远程数据库的IP，port表示远程数据库的端口号,-U 后面跟要连接的用户信息
gsql -U omm -h xx.xx.xx.xx -p port -d postgres -r
FATAL: Forbid remote connection with initial user.
~~~


### Anwser

远程连接openGauss数据库不能使用初始化用户（omm用户），必须使用创建的其他用户连接才可以

