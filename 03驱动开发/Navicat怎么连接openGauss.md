### Question

~~~
Navicat工具怎么连接远程openGauss数据库？
~~~


### Anwser

首先初始用户（omm）是不能远程连接的！！！

需要修改对应的配置文件

~~~shell
#pg_hba.conf中增加白名单，密码校验方式为MD5，0.0.0.0/0表示放行所有的IP，建议改为信任的IP
host all all 0.0.0.0/0 md5
#postgresql.conf
password_encrytion_type=1
~~~

修改完成之后重启数据库

然后创建新用户，用新创建的用户去连接数据库，以下是命令模板：

~~~sql
create database testdb;
\c testdb
create user test password 'test@123';
grant all PRIVILEGES to test;
alter database testdb owner to test;
~~~

使用postgresql驱动去连接，用户选择test，数据库可以选择postgres或者testdb

另外，需要注意数据库的端口需要在服务器上放行。