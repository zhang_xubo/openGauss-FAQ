#!/bin/bash

#######################################################
# 
# 本地执行清理指定目录下的日志文件
#
#######################################################

set -e
STORAGE_DAYS=90
current_date=$(date +%Y-%m-%d_0000 --date="-${STORAGE_DAYS} day")

function main()
{
    logfile=$1
    logpath=$2
    STORAGE_DAYS=$3
    current_date=$(date +%Y-%m-%d_0000 --date="-${STORAGE_DAYS} day")
    current_date_file="${logfile}-${current_date}"

    arr=(`ls ${logpath} | grep -E ".log|.log.gz" | sort`)
    for file in ${arr[@]}
    do
        if [[ "$current_date_file" > "$file" ]]; then
            echo $file
            rm -f "${logpath}/$file"
        fi
    done
}

main $@

