### Question

使用gs_checkperf工具检查数据库性能的时候报错，报错如下：

~~~
[omm@testos]$gs_checkperf
[GAUSS-51300] : Failed to execute SQL: /gauss/gaussdb/app/bin/gsql -U omm -p 15400 -d postgres -X --variable=ON_ERROR_STOP=on -f "/gauss/gaussdb/app/share/postgresql/pmk_schema_bak.sql". Error:
START TRANSACTION
gsql:/gauss/gaussdb/app/share/postgresql/pmk_schema_bak.sql:4: ERROR:  cannot execute CREATE SCHEMA in a read-only transaction
total time: 6  ms
~~~



### Anwser

如上提示该命令应该使用在主备环境的主节点上，而非备节点上。



